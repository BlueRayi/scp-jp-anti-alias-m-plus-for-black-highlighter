// ==UserScript==
// @name         SCP-JP: Anti Alias M Plus for Black Highlighter
// @namespace    https://junjo-ponpo.com/
// @version      $version
// @description  $description
// @author       BlueRayi
// @match        *://scp-jp.wikidot.com/*
// @icon         https://scp-wiki.wikidot.com/local--favicon/favicon.gif
// @grant        none
// @license GPL-3.0-or-later; https://www.gnu.org/licenses/gpl-3.0.txt
// ==/UserScript==
