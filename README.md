# SCP-JP: Anti Alias M Plus for Black Highlighter

SCP 財団日本支部版 [Black Highlighter](http://scp-jp.wikidot.com/theme:black-highlighter-theme) テーマで本文に使用されている M Plus を一部環境で表示させたときに生じるジャギーを回避するために、それらの要素をわずかに傾けることでアンチエイリアスを発生させるユーザースクリプトです。

Windows 環境かつ FHD 前後の解像度のディスプレイを利用している場合、本スクリプトは特に効果を発揮するでしょう。

<img src="before_after.gif" alt="スクリプト適用前後のプレビュー。適用前でジャギーが発生して読みづらい文字が、適用後はアンチエイリアスがかかり読みやすいくなっている" width="740" />

「ポ」や「で」といった文字で顕著に発生していたジャギーが解消されているのがわかります。

## 使い方

1. Tampermonkey[^tampermonkey]などのユーザースクリプトを導入する拡張機能をブラウザに導入します。
2. [Greasy Fork のスクリプトページ](https://greasyfork.org/ja/scripts/463751-scp-jp-anti-alias-m-plus-for-black-highlighter)にアクセスし、インストールボタンをクリックします。
3. WAN に祈りと感謝を捧げます。

もしくは、

1. Tampermonkey[^tampermonkey]などのユーザースクリプトを導入する拡張機能をブラウザに導入します。
2. 本リポジトリを手元にクローンします。
  ```shell
  git clone https://gitlab.com/BlueRayi/scp-jp-anti-alias-m-plus-for-black-highlighter
  ```
3. Dart 環境で `bin/build.dart` を実行します。
  ```shell
  dart bin/build.dart
  ```
4. 出力された `build/userscript.js` の内容をコピーし拡張機能のダッシュボードから新規スクリプトとして追加します。
5. WAN に祈りと感謝を捧げます。

## 動作の仕組み

ページの CSS 変数 `--body-font` が `M Plus` を含んでいる場合[^body-font]、HTML の BODY 要素を掘っていって、以下に示す基準を満たす HTML 要素をランダムに右か左[^coin]に 0.05°傾けます。

### 対象要素名

* `h1` - `h6` … 見出しです。
* `table` … 表です。
* `p` … 一般的な本文です。

### 対象要素 ID

N/A

### 対象要素クラス

* `.collapsible-block-link` … 折りたたみ構文の開閉リンクです。
* `.footnote-footer` … 脚注の文面です。
* `.top-left-box` … ACS におけるアイテム番号部分です。
* `.top-right-box` … ACS におけるセキュリティクリアランス部分です。
* `.class-category` … ACS における各クラスの分類です。
* `.class-text` … ACS における各クラスの本文です。
* `.wikisys--template-dialog` … カスタムテーマページの冒頭欄です。
* `.fonts-display` … カスタムテーマページのフォント情報欄です。

[^tampermonkey]: ブラウザごとに [Edge 版](https://microsoftedge.microsoft.com/addons/detail/tampermonkey/iikmkjmpaadaobahmlepeloendndfphd) / [Chrome 版](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) / [Firefox 版](https://addons.mozilla.org/ja/firefox/addon/tampermonkey/)があります。[Safari 版](https://apps.apple.com/us/app/tampermonkey/id1482490089)も存在しますが、このスクリプトが解決する問題が発生するのは Windows 環境に限られていると考えられますので、積極的に導入する必要はありません。

[^body-font]: この変数が存在することは Black Highlighter テーマの特徴のひとつです。いっぽう、Black Highlighter から派生したすべてのテーマが M Plus フォントを採用しているわけではないため、このように条件づけています。

[^coin]: 傾きの方向がランダムなのはすべてを同じ方向に傾けた場合全体が傾いているようにみえる可能性があると考えたためですが、おそらく私の杞憂です。