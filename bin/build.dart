import 'dart:core';
import 'dart:io';
import 'dart:convert';

import 'package:path/path.dart' as path;
import 'package:yaml/yaml.dart' show loadYaml;

const utf8 = Utf8Codec();

void main() async {
  final selfPath = Platform.script.toFilePath();
  final projectPath = path.dirname(path.dirname(selfPath));
  final binPath = path.join(projectPath, 'bin');
  final resourcesPath = path.join(projectPath, 'resources');
  final buildPath = path.join(projectPath, 'build');
  final pubspecPath = path.join(projectPath, 'pubspec.yaml');
  final sourcePath = path.join(binPath, 'aamplus.dart');
  final headerPath = path.join(resourcesPath, 'header.js');
  final bodyPath = path.join(buildPath, 'body.js');
  final buildedPath = path.join(buildPath, 'userscript.js');

  final build = Directory(buildPath);
  final pubspec = File(pubspecPath);
  final source = File(sourcePath);
  final header = File(headerPath);
  final body = File(bodyPath);
  final builded = File(buildedPath);

  final scriptContent = await getScript(build, pubspec, header, source, body);
  builded.writeAsString(scriptContent);
}

Future<String> getScript(Directory outputs, File pubspec, File header,
    File source, File body) async {
  final contents = await Future.wait(
      [getHeader(pubspec, header), getBody(outputs, source, body)]);
  return '${contents[0]}\n${contents[1]}';
}

Future<String> getHeader(File pubspec, File header) async {
  final contents =
      await Future.wait([pubspec.readAsString(), header.readAsString()]);
  final pubspecContents = PubSpec(loadYaml(contents[0]));
  return contents[1]
      .replaceAll(RegExp(r'\$description'), pubspecContents.description ?? '')
      .replaceAll(RegExp(r'\$version'), pubspecContents.version ?? '');
}

Future<String> getBody(Directory outputs, File source, File body) async {
  await outputs.create(recursive: true);
  await Process.run(
      'dart',
      [
        'compile',
        'js',
        source.path,
        '-o',
        body.path,
        '-O4',
        '--no-source-maps'
      ],
      stdoutEncoding: utf8,
      stderrEncoding: utf8);
  final bodyContent = await body.readAsString();
  body.delete();
  File('${body.path}.deps').delete();
  return bodyContent;
}

class PubSpec {
  final dynamic raw;

  PubSpec(this.raw);

  String? get description {
    return raw['description'];
  }

  String? get version {
    return raw['version'];
  }
}
