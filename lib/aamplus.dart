extension SetAlgebra<E> on Set<E> {
  Set<E> operator &(Set<Object?> other) {
    return intersection(other);
  }

  Set<E> operator ^(Set<E> other) {
    return union(other).difference(intersection(other));
  }

  Set<E> operator |(Set<E> other) {
    return union(other);
  }

  Set<E> operator -(Set<Object?> other) {
    return difference(other);
  }
}
