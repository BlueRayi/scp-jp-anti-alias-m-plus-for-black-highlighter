import 'dart:core';
import 'dart:html';
import 'dart:math';

import 'package:aamplus/aamplus.dart';

final random = Random();

void main() {
  final body = document.body;
  if (body == null) {
    return;
  }
  final bodyFont = body.getComputedStyle().getPropertyValue('--body-font');
  if (bodyFont.contains('M Plus')) {
    antiAliasFont(body);
  }
}

void antiAliasFont(Element element) {
  if (isAATarget(element)) {
    final coin = random.nextBool();
    final rotate = coin ? '0.05deg' : '-0.05deg';
    final transform = 'rotate($rotate)';
    element.style.transform = transform;
  } else {
    element.children.forEach(antiAliasFont);
  }
}

bool isAATarget(Element element) {
  final targetNames = {
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'table',
    'p',
  };
  final targetIds = <String>{};
  final targetClasses = {
    'collapsible-block-link',
    'footnote-footer',
    'top-left-box',
    'top-right-box',
    'class-category',
    'class-text',
    'wikisys--template-dialog',
    'fonts-display',
  };
  return targetNames.contains(element.tagName.toLowerCase()) ||
      targetIds.contains(element.id) ||
      (element.classes & targetClasses).isNotEmpty;
}
